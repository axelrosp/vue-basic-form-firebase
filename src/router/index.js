import { createRouter, createWebHistory } from 'vue-router'
import store from '../store'

const routes = [
    {
        path: '/',
        name: 'Home',
        component: () => import('../views/Home.vue'),
        meta: { protectedRoute: true }
    },
    {
        path: '/login',
        name: 'Login',
        component: () => import('../views/Login.vue')
    },
    {
        path: '/register',
        name: 'Register',
        component: () => import('../views/Register.vue')
    },
    {
        path: '/edit/:id',
        name: 'Edit',
        component: () => import('../views/Edit.vue'),
        meta: { protectedRoute: true }
    }
]

const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes
})

router.beforeEach((to, from, next) => {
    if (to.meta.protectedRoute) {
        if (store.getters.isUserAuthenticated) {
            next()
        } else {
            next('/login')
        }
    }
    else next()
})

export default router
