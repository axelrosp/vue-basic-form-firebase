import * as taskController from "./taskController"
import * as authController from "./authController"

export {
    taskController,
    authController
}