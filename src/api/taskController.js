import { firebaseConfig } from "./config"

const API_URL_TASKS = `${firebaseConfig.databaseURL}/tasks`

export async function fetchTasks(user) {
    const res = await fetch(`${API_URL_TASKS}/${user.localId}.json?auth=${user.idToken}`)
    return await res.json()
}

export async function putTask(user, task) {
    const res = await fetch(`${API_URL_TASKS}/${user.localId}/${task.id}.json?auth=${user.idToken}`, {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(task)
    })
    return await res.json()
}

export async function updateTask(user, task) {
    const res = await fetch(`${API_URL_TASKS}/${user.localId}/${task.id}.json?auth=${user.idToken}`, {
        method: 'PATCH',
        body: JSON.stringify(task)
    })
    return await res.json()
}

export async function deleteTask(user, id) {
    return await fetch(`${API_URL_TASKS}/${user.localId}/${id}.json?auth=${user.idToken}`, {
        method: 'DELETE',
    })
}
