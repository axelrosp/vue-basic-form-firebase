import { createStore } from 'vuex'
import router from '../router'
import { taskController, authController } from '../api'
import { defaultTask } from '../models/taskModel'

export default createStore({
    state: {
        tasks: [],
        task: { ...defaultTask },
        user: null,
        error: null
    },
    mutations: {
        setError(state, payload) {
            if (!payload) {
                return state.error = null
            }
            //Login
            if (payload === "EMAIL_NOT_FOUND") {
                return state.error = { type: 'email', message: 'Email no registrado' }
            }
            if (payload === "INVALID_PASSWORD") {
                return state.error = { type: 'password', message: 'Password incorrecta' }
            }
            //Register
            if (payload === "EMAIL_EXISTS") {
                return state.error = { type: 'email', message: 'Email ya registrado' }
            }
            if (payload === "INVALID_EMAIL") {
                return state.error = { type: 'email', message: 'Formato incorrecto de email' }
            }
        },
        setUser(state, payload) {
            state.user = payload
        },
        fetchTasks(state, payload) {
            state.tasks = payload
        },
        setTask(state, payload) {
            state.tasks = [payload, ...state.tasks]
        },
        deleteTask(state, payload) {
            state.tasks = state.tasks.filter(task => task.id !== payload)
        },
        setSelectedTask(state, paylaod) {
            const task = state.tasks.find(task => task.id === paylaod)
            if (!task) {
                router.push('/')
            }
            else state.task = task
        },
        updateTask(state, payload) {
            state.tasks.map(task => task.id === payload.id ? payload : task)
            router.push('/')
        }
    },
    actions: {
        logoutAction({ commit }) {
            commit('setUser', null)
            localStorage.removeItem('user')
        },
        async registerUserAction({ commit }, user) {
            const userDb = await authController.registerUser(user)
            if (userDb.error) {
                commit('setError', userDb.error.message)
            }
            else {
                commit('setUser', userDb)
                commit('setError', null)
                localStorage.setItem('user', JSON.stringify(userDb))
            }
        },
        async loginUserAction({ commit }, user) {
            const userDb = await authController.loginUser(user)
            if (userDb.error) {
                commit('setError', userDb.error.message)
            }
            else {
                commit('setUser', userDb)
                commit('setError', null)
                localStorage.setItem('user', JSON.stringify(userDb))
            }
        },
        async fetchTasksAction({ commit, state }) {
            if (state.user) {
                try {
                    const tasksData = await taskController.fetchTasks(state.user)
                    if (tasksData) {
                        const tasks = Object.keys(tasksData).map(key => (tasksData[key]))
                        commit('fetchTasks', tasks)
                    }
                    else {
                        commit('fetchTasks', [])
                    }
                } catch (error) {
                    console.error(error)
                }
            }
        },
        async setTaskAction({ commit, state }, task) {
            if (state.user) {
                try {
                    await taskController.putTask(state.user, task)
                    commit('setTask', task)
                } catch (error) {
                    console.error(error)
                }
            }
        },
        async updateTaskAction({ commit, state }, task) {
            if (state.user) {
                try {
                    await taskController.updateTask(state.user, task)
                    commit('updateTask', task)
                } catch (error) {
                    console.error(error)
                }
            }
        },
        async deleteTaskAction({ commit, state }, id) {
            if (state.user) {
                try {
                    await taskController.deleteTask(state.user, id)
                    commit('deleteTask', id)
                } catch (error) {
                    console.error(error)
                }
            }
        },
        loadLocalStorage({ commit, state }) {
            if (!state.user) {
                const user = localStorage.getItem('user')
                if (user) commit('setUser', JSON.parse(user))
                else commit('setUser', null)
            }
        },
        setSelectedTaskAction({ commit }, id) {
            commit('setSelectedTask', id)
        },
    },
    getters: {
        isUserAuthenticated(state) {
            return !!state.user
        }
    },
    modules: {
    }
})
